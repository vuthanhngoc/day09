<?php
require_once 'const.php';
$selectedAnswers = array(); // answer of user

// handle user post submit, next, prev page 
if ($_SERVER['REQUEST_METHOD'] === "POST") {
    if (isset($_POST['nextPage'])) {
        if (isset($_COOKIE['page'])) {
            setcookie('page', $_COOKIE['page'] + 1);
            $_COOKIE['page'] = $_COOKIE['page'] + 1;
            if (isset($_COOKIE['answer'])) {
                $selectedAnswers = json_decode($_COOKIE['answer'], true);
                $selectedAnswers = $_POST + $selectedAnswers;
                setcookie('answer', json_encode($selectedAnswers));
            } else {
                setcookie('answer', json_encode($_POST));
            }
        } else {
            setcookie('page', 1);
            $_COOKIE['page'] = 1;
        }
    }
    if (isset($_POST['prev'])) {
        if (isset($_COOKIE['page'])) {
            setcookie('page', $_COOKIE['page'] - 1);
            $_COOKIE['page'] = $_COOKIE['page'] - 1;
            $selectedAnswers = json_decode($_COOKIE['answer'], true);
            $selectedAnswers = $_POST + $selectedAnswers;
            setcookie('answer', json_encode($selectedAnswers));
        } else {
            setcookie('page', 1);
            $_COOKIE['page'] = 1;
        }
    }
}
if (!isset($_COOKIE["page"]) || $_COOKIE['page'] <= 0) {
    setcookie('page', 1);
    $_COOKIE['page'] = 1;
}
function isNextPage($array, $end)
{
    return true ? $end < count($array) : false;
}
function isChoiceUser($selectedAnswers, $keyQuestion, $answer)
{
    if (isset($selectedAnswers[$keyQuestion]) && $selectedAnswers[$keyQuestion] == $answer) {
        return true;
    }
    return false;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    :root {
        --bg-color:#1068BF;
        --text-color: #fff;
        --btn-color:#1068BF;
    }

    body[data-theme="dark"] {
        --bg-color:#000;
        --text-color: #fff;
        --btn-color: #1068BF;
    }

    body[data-theme="light"] {
        --bg-color: #fff;
        --text-color: #000;
        --btn-color: #72b626;
    }

    body {
        background-color: var(--bg-color);
        color: var(--text-color);
        padding: 10px;
        font-family: monospace;
        transition: 0.5s;
    }

    .questions {
        width: 80%;
        /* text-align: center; */
        margin: auto;
    }

    .wrap-question {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
    }

    .question {
        width: 48%;
        margin: 5px;
        border: 1px solid #999;
        border-radius: 5px;
        padding: 8px;
    }

    .question h3 {
        margin-bottom: 5px;
        font-size: 18px;
    }

    .question span {
        text-decoration: underline;
        color: var(--btn-color);
    }

    .question .answer {
        font-size: 17px;
        margin-bottom: 3px;
    }

    label,
    button,
    input {
        cursor: pointer;
    }

    .action {
        display: flex;
        justify-content: center;
        margin-top: 16px;
    }

    input[type="submit"] {
        padding: 10px 8px;
        background: var(--btn-color);
        outline: none;
        border-radius: 10px;
        border: none;
        color: #fff;
        font-size: 18px;
        text-align: center;
        transition: 0.3s;
        margin-left: 6px;

    }

    input[type="submit"]:hover {
        transform: scale(1.03);

    }

    .theme {
        position: absolute;
        bottom: 50px;
        right: 50px;
        width: 60px;
        height: 60px;
        z-index: 1;
        text-align: center;
        font-weight: 600;
        border-radius: 50%;
        border: none;
        outline: none;
        color: var(--bg-color);
        text-transform: uppercase;
        background-color: var(--text-color);
        transition: 0.2s;

    }

    .theme:hover {
        transform: scale(1.09);
        transition: 0.2s;

    }
    </style>
    <script>
    window.addEventListener('DOMContentLoaded', (event) => {
        let theme = document.querySelector('.theme');

        if (localStorage.getItem('data-theme')) {
            document.body.setAttribute('data-theme', localStorage.getItem('data-theme'))
        }
        theme.addEventListener('click', (e) => {
            let body = document.body;
            let themeType = body.getAttribute('data-theme') && (body.getAttribute('data-theme') ==
                'dark') ? 'light' : 'dark';
            theme.innerHTML = body.getAttribute('data-theme');
            body.setAttribute('data-theme', themeType);
            localStorage.setItem('data-theme', themeType);
        })
    });
    </script>
</head>

<body data-theme="dark">
    <button class='theme'>Dark</button>
    <form method="POST" action="submit.php">
        <div class="questions">
            <div class="wrap-question">
                <?php $questionNum = $_COOKIE['page'] * LIMIT - LIMIT;
                foreach (array_slice(QUESTIONS, ($_COOKIE['page'] - 1) * LIMIT, LIMIT, true) as $keyQuestion => $question) :
                    $questionNum = $questionNum  + 1;
                ?>
                <div class="question">
                    <h3><span>Cau hoi <?= $questionNum ?>:</span> <?= $question ?>
                    </h3>
                    <div class="answer">
                        <?php foreach (ANSWERS[$keyQuestion]['answers'] as $answer) : ?>
                        <input type="radio" id="<?= $answer . $question ?>" name="<?= $keyQuestion ?>"
                            value="<?= $answer ?>"
                            <?php if (isChoiceUser($selectedAnswers, $keyQuestion, $answer)) echo 'checked'; ?>>
                        <label for="<?= $answer . $question ?>"> <?= $answer ?> </label><br>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php endforeach; ?>


            </div>
            <div class="action">
                <?php
                if (isNextPage(QUESTIONS, $questionNum)) {
                    echo '<input type="submit" name="nextPage" value="Trang tiep theo" formaction="" formmethod="POST">';
                } else {
                    echo '<input type="submit" name="prev" value="Quay lai" formaction="" formmethod="POST" >';
                    echo '<input type="submit" name="submit" value="Nop bai">';
                }
                ?>

            </div>
        </div>
    </form>
</body>

</html>