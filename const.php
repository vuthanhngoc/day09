<?php
define("QUESTIONS", array(
    '0' => '2k1 là biết tắt của ?', '1' => '2 * 2 = ?',
    '2' => '3 * 3 = ?', '3' => '4 + 2 = ?',
    '4' => '5 - 3 = ?', '5' => '1 + 6 = ?',
    '6' => '2 / 2 = ?', '7' => '3 + 1 = ?',
    '8' => '2 x 5 = ?', '9' => '5 + 0 = ?',
));
define("ANSWERS", array(
    '0' => array('answers' => array(2001, 2002,2003,2004), 'result' => 2001),
    '1' => array('answers' => array(2, 3, 4, 5), 'result' => 4),
    '2' => array('answers' => array(2, 9, 4, 5), 'result' => 9),
    '3' => array('answers' => array(2, 3, 4, 6), 'result' => 6),
    '4' => array('answers' => array(1, 2, 6, 9), 'result' => 2),
    '5' => array('answers' => array(7, 2, 5, 0), 'result' => 7),
    '6' => array('answers' => array(2, 3, 4, 1), 'result' => 1),
    '7' => array('answers' => array(2, 3, 4, 5), 'result' => 4),
    '8' => array('answers' => array(2, 3, 10, 5), 'result' => 10),
    '9' => array('answers' => array(2, 0, 15, 5), 'result' => 5),
));
define("LIMIT", 5);